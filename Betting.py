import numpy as np


class Betting:
    # declare class variables
    tradingFee = 0.0025
    currencyPairs = []  # assume format of "BTC_%", where % is the ticker name of the coin we are might trade (example: BTC_ETH)
    currencyPairsSortedByGain = []
    probabilityCurrencyPricesGoUp = np.matrix(0)  # to-do: figure out if these lines are needed
    probabilityCurrencyPricesGoDown = np.matrix(0)
    numberOfCurrencyComparisons = np.matrix(0)
    expectedGainsForEachCurrency = np.matrix(0)
    expectedGainsForEachCurrencyNoTrade = np.matrix(0)
    fractionOfBankrollToInvestInEachCurrency = np.matrix(0)
    fractionOfBankrollToInvestSortedByGain = np.matrix(0)

    # define the class constructor
    def __init__(self, tradingFee, currencyPairs):
        self.tradingFee = tradingFee
        self.currencyPairs = currencyPairs
        self.probabilityCurrencyPricesGoUp = self.createMatrix(len(currencyPairs), len(currencyPairs), 0.5)
        self.probabilityCurrencyPricesGoDown = self.createMatrix(len(currencyPairs), len(currencyPairs), 0.5)
        self.numberOfCurrencyComparisons = self.createMatrix(len(currencyPairs), len(currencyPairs), 0)
        self.expectedTradeProfits = np.zeros(len(currencyPairs))

    # define the class methods
    def createMatrix(self, numberOfRows, numberOfColumns, scalarToFillWith):
        # return an n X n matrix, where all elements are set to zero
        matrix = np.zeros(shape=(numberOfRows, numberOfColumns))
        matrix.fill(scalarToFillWith)
        return matrix

    def incrementNumberOfCurrencyComparisons(self, firstCurrencyPair, secondCurrencyPair):
        firstIndex = self.currencyPairs.index(firstCurrencyPair)
        secondIndex = self.currencyPairs.index(secondCurrencyPair)
        self.numberOfCurrencyComparisons[firstIndex, secondIndex] += 1
        self.numberOfCurrencyComparisons[secondIndex, firstIndex] += 1

    def initializeMatricesWithHistoricalData(self, downSameUpDataForCurrencies):
        # NOTE: the downSameUpDataForCurrencies is a 2D array. it's not in the right format to be immediately useful.
        # one "row" in this 2D array is data for one currency over time. one "column" is a snapshot of all currencies at one point in time.

        numberOfDataPoints = len(downSameUpDataForCurrencies[0])

        for i in range(numberOfDataPoints - 1):
            oneChangeSetForAllCurrencies = []
            list1 = []
            list2 = []
            for downSameUpData in downSameUpDataForCurrencies:
                list1.append(downSameUpData[i])
                list2.append(downSameUpData[i + 1])
            oneChangeSetForAllCurrencies.append(list1)
            oneChangeSetForAllCurrencies.append(list2)
            self.conditionalProbability(oneChangeSetForAllCurrencies)

    def printClassVariables(self):
        print('current values of Betting class variables')
        print('\n tradingFee: ')
        print(self.tradingFee)
        print('\n currencyPairs: ')
        print(self.currencyPairs)
        print('\n currencyPairsSortedByGain: ')
        print(self.currencyPairsSortedByGain)
        print('\n probabilityCurrencyPricesGoUp: ')
        print(self.probabilityCurrencyPricesGoUp)
        print('\n probabilityCurrencyPricesGoDown: ')
        print(self.probabilityCurrencyPricesGoDown)
        print('\n numberOfCurrencyComparisons: ')
        print(self.numberOfCurrencyComparisons)
        print('\n expectedGainsForEachCurrency: ')
        print(self.expectedGainsForEachCurrency)
        print('\n fractionOfBankrollToInvestInEachCurrency: ')
        print(self.fractionOfBankrollToInvestInEachCurrency)
        print('\n fractionOfBankrollToInvestSortedByGain')
        print(self.fractionOfBankrollToInvestSortedByGain)

    def conditionalProbability(self, latest2ChangesInCurrencies):  # Computes conditional propabilities that stocks will go up given latest outcome of all stocks.
        # Assumes stocks only have 2 outcomes: went up, or went down.
        # The variables used in the equations are:
        # N is the array of the stock names in alphabetical order
        # Q is a list of 2 vectors of the latest 2 outcomes of each stock (up or down).
        # T is a matrix of number of times 2 stocks have been looked at together (should be symmetric)
        # pUp is a matrix of conditional probabilities that each stock will go up given another stock went up (assumed symmetric).
        # pDown is a matrix of conditional probabilities that each stock will go up given another stock went down (assumed symmetric).
        # comm is the commission as a fraction of investment in a given stock (if fixed rate commission, need to update with that option).

        # Initially you want to run this starting with T = pUp = pDown = zeros or empty, without betting for some time to get initial T, pUp, and pDown.

        # for brevity of formulas, map method parameter and class variables to local variables with short names
        N = self.currencyPairs
        Q = latest2ChangesInCurrencies
        T = self.numberOfCurrencyComparisons
        pUp = self.probabilityCurrencyPricesGoUp
        pDown = self.probabilityCurrencyPricesGoDown
        f0 = self.fractionOfBankrollInvestedInCurrencies
        G = self.createDictionaryOfCurrencyPairs()  # expected gains
        G0 = self.createDictionaryOfCurrencyPairs()  # expected no trade gains
        P = self.createDictionaryOfCurrencyPairs()  # probabilities that stocks will go up
        f = self.createDictionaryOfCurrencyPairs()  # the fractions of the bankroll to invest in each stock to ma

        if len(Q[0]) != len(Q[1]):
            print("ERROR: Q's not of equal length:")
            print(len(Q[0]), len(Q[1]))

        varG = np.zeros(len(Q[0]))  # expected variances on gains

        pUpNew = np.copy(pUp)  # updated pUp

        pDownNew = np.copy(pDown)  # updated pDown

        # For each stock look at each conditional probability of going up given latest outcomes, Q, of all stocks.
        for i in range(len(Q[0])):
            # Look at probability ith stock will go up
            for j in range(len(Q[1])):

                # Get conditional probabilities that ith stock will go up, given latest outcome, Q, of jth stock.

                if Q[1][j] > 0.:
                    # If jth stock went up, then use pUp
                    P[N[i]] += pUp[i, j]

                    # add contribution to gain variance from jth stock
                    if pUp[i, j] > 1. or pUp[i, j] < 0.:
                        print("ERROR: pDown is out of range: %f" % pUp[i, j])

                    if pUp[i, j] == 1. or pUp[i, j] == 0.:
                        varG[i] += 0.
                    else:
                        varG[i] += pUp[i, j] * (1. - pUp[i, j]) / (T[i, j] + 1.0) * (np.log(
                            pUp[i, j] / (1.0 - pUp[i, j]))) ** 2

                    if Q[0][i] > 0.:
                        pUpNew[i, j] = (T[i, j] * pUp[i, j] + 1.) / (T[i, j] + 1.)
                    elif Q[0][i] < 0.:
                        pDownNew[i, j] = (T[i, j] * pDown[i, j] + 1.) / (T[i, j] + 1.)
                        pDownNew[j, i] = pDownNew[i, j]  # Assume pDown symmetric

                elif Q[1][j] < 0.:
                    # If jth stock went down, then use pDown

                    P[N[i]] += pDown[i, j]

                    if pDown[i, j] > 1. or pDown[i, j] < 0.:
                        print("ERROR: pDown is out of range: %f" % pDown[i, j])

                    # add contribution to gain variance from jth stock
                    if pDown[i, j] == 1. or pDown[i, j] == 0.:
                        varG[i] += 0.
                    else:
                        varG[i] += pDown[i, j] * (1. - pDown[i, j]) / (T[i, j] + 1.) * (np.log(
                            pDown[i, j] / (1. - pDown[i, j]))) ** 2

                    if Q[0][i] > 0.:
                        pUpNew[i, j] = T[i, j] * pUp[i, j] / (T[i, j] + 1.)
                        pUpNew[j, i] = pUpNew[i, j]  # Assume pUp symmetric
                    elif Q[0][i] < 0.:
                        pDownNew[i, j] = T[i, j] * pDown[i, j] / (T[i, j] + 1.)

                else:
                    # just in case jth stock stayed the same assume P=50/50.
                    # Don't use as data point in this case.
                    P[N[i]] += .5

                # Update T ignoring Q = 0 cases.
                if Q[0][i] * Q[1][j] != 0.:
                    T[i, j] += 1.

            P[N[i]] = P[N[i]] / float(len(Q[0]))  # renormalize P

            f[N[i]] = (2. * P[i] - 1.)  # fraction of bankroll to invest.

            if P[N[i]] == 0. or P[N[i]] == 1.:
                G[N[i]] = np.log(2.)
            else:
                G[N[i]] = np.log(2.) + P[N[i]] * np.log(P[N[i]]) + (1. - P[N[i]]) * np.log(1. - P[N[i]])

            if P[N[i]] < 0. or P[N[i]] > 1.:
                print("ERROR: P[i] out of range: %f" % P[N[i]])

            # if (G[i] - np.sqrt(varG[i] / (float(len(Q[0])) - 1.))) <= 0.:
            if G[N[i]] <= 0.:  # if gain less then 0, don't bet.

                f[N[i]] = 0.
                G[N[i]] = 0.

            if f0[N[i]]==1.:
                G0[N[i]]=2.*P[N[i]]-1.5

            else:
                G0[N[i]] = P[i]*np.log(1.+f0[i])+(1.-P[i])*np.log(1.-f0[i])

        # update the class variables
        self.numberOfCurrencyComparisons = T
        self.probabilityCurrencyPricesGoUp = pUpNew
        self.probabilityCurrencyPricesGoDown = pDownNew
        self.expectedGainsForEachCurrency = G
        self.expectedGainsForEachCurrencyNoTrade = G0
        self.fractionOfBankrollToInvestInEachCurrency = f

        # Now send f to a code (BuySell) that buys/sells f(i)*(bankroll) of each stock.

    def buySell(self, candlesperround,short=False):  # determines how much to buy or sell in each stock.
        # f0 is the current fraction of bankroll in each stock, also ordered in G.
        # f1 is fraction of bankroll (cash) to buy/sell in a given stock, also ordered as G.
        # cash is your bankroll
        # short is the option to sell shares you don't own.  Takes values 0 (off) or 1 (on). If short=0, then off.

        # for brevity of formulas, map class variable to local variable with short name

        N = self.currencyPairs
        Bank = self.calculateTotalValueOfBankroll
        f0 = self.fractionOfBankrollInvestedInCurrencies
        f1 = self.fractionOfBankrollToInvestInCurrencyPairs
        comm=self.tradingFee
        G1 = self.expectedGainsForEachCurrency
        G0 = self.expectedGainsForEachCurrencyNoTrade
        G1 = candlesperround*G1
        G0 = candlesperround*G0
        # Under construction.  Short option not included yet.

        if short == False:  # short off

            buy = []  # list of amount to buy in stocks.
            sell = []  # list of amount to sell in stocks.
            cash = 0.


         # covert all negative f1's to 0.
            for i in range(len(N)):
                coin = N[i]
                if f1[coin]<0.:

                    f1[coin] = 0.
                    G1[coin] = 0.

            Banknew = 1.-comm*sum(abs(f1-f0))
            Bank1=Bank*Banknew # Bankroll after transactions

        # Check to see which coins are worth selling given transaction fee
            for i in range(len(N)):
                coin = N[i]
                df = abs(Banknew(f1[coin]-f0[coin]))

                if G1[coin]-comm*df > G0[coin]:
                    G1[coin]=G1[coin]-comm*df

                    if G1[coin]<=0.:
                        G1[coin] = 0.
                        f1[coin] = 0.

                elif G0[coin]>0.:
                    G1[coin] = G0[coin]
                    f1[coin] = f0[coin]/Banknew
                else:
                    G1[0] = 0.
                    f1[0] = 0.


        # Get cash from selling all coins to be sold
            for i in range(len(N)):

                coin = N[i]
                if G1[coin] == 0. and f0[coin] !=0.:

                    sell.append([N[i],Bank*f0[i]])
                    cash+=Bank*f0[i]*(1.-comm)

                elif G1[coin]>0. and f1[coin]<f0[coin]:

                    sell.append([N[i],Bank*(f0[coin]-Banknew*f1[coin])])
                    cash+=Bank*(f0[coin]-Banknew*f1[coin])(1.-comm)



        # Now that we have cash, buy in order of highest gain until run out of cash
            i = G1.index(np.max(G1))
            coin = N[i]
            df = Banknew * f1[coin] - f0[coin]

            if cash-Bank1*f1[coin]<0. and G1[coin]>0.:
                done = 1

            else:
                done = 0

            while done == 0:


                buy.append([N[i],Bank*df])
                cash-=Bank1*f1[coin]
                G1[coin] = 0.

                i = G1.index(np.max(G1))
                coin = N[i]
                df = Banknew * f1[coin] - f0[coin]

                if cash - Bank1*f1[coin] < 0. and G1[coin] > 0.:
                    done = 1

                else:
                    done = 0


        # If leftover cash, either buy part next coin in gain if worthwhile, or else add to Bitcoin
        # Actually this loop would probably be better after actually having done the trades.
            if cash>0.:

                extra = Bank-cash

                if G1[coin]>0.:

                    buy.append([N[i],extra/(1.+comm)])

                elif 'BTC_BTC' in buy:

                    i = buy.index('BTC_BTC')/2
                    buy[i][1] = buy[i][1]+extra/(1.+comm)

                elif 'BTC_BTC' in sell:

                    i = sell.index('BTC_BTC') / 2
                    sell[i][1] = sell[i][1] - extra / (1. + comm)

                else:

                    buy.append(['BTC_BTC',extra / (1. + comm)])



        return buy, sell

        # Now sell stocks in Sell 1st, and then buy stocks in Buy.  Note Sell values are positive.

    def calculateTotalValueOfBankroll(self):

        btcValueOfAllAssets = 0.0  # in units of bitcoin
        for key in self.currentBalances:
            btcValueOfThisCurrency = float(self.currentBalances[key]['btcValue'])
            btcValueOfAllAssets += btcValueOfThisCurrency

        return btcValueOfAllAssets

    def calculateFractionOfBankrollInvestedFromCurrentBalances(self, currentBalances):
        btcValueOfAllAssets = 0.0  # in units of bitcoin
        for key in currentBalances:
            btcValueOfThisCurrency = float(currentBalances[key]['btcValue'])
            btcValueOfAllAssets += btcValueOfThisCurrency

        fractionOfBankrollInvestedInEachCurrency = {}
        for key in currentBalances:
            fractionOfBankrollInvestedInEachCurrency[key] = float(currentBalances[key]['btcValue']) / btcValueOfAllAssets

        self.fractionOfBankrollInvestedInCurrencies = fractionOfBankrollInvestedInEachCurrency
