from Betting import Betting
from PoloniexDataProcessor import PoloniexDataProcessor
from DataPlotter import DataPlotter

import datetime
import json
import sys
import urllib.request


def main(argv):
    # to-do: map arguments (argv) to variables
    # to-do: add a "debugging mode" boolean. if True, then we print variables and potentially useful debugging info to the console

    # figure out which currencies we want to look at based on the volume traded in the last 24 hours
    # volumeThresholdInLast24Hours = 800.0  # in units of bitcoin
    # currencyPairNames = dataProcessor.getCurrencyPairsAboveThreshold(volumeThresholdInLast24Hours)
    currencyPairs = ['BTC_BTS', 'BTC_DASH', 'BTC_LTC', 'BTC_NXT', 'BTC_ETH', 'BTC_BCH']
    # print('\n there are %d' % len(currencyPairNames) + ' currency pairs above the 24 hour volume threshold of %.2f' % volumeThresholdInLast24Hours + ' bitcoins')

    # process candlestick data (in 300 second intervals) for the last hour (3600 seconds) for bitcoin in terms of US dollars
    allowedPeriods = [300, 900, 1800, 7200, 14400, 86400]  # periods allowed by Poloniex
    period = allowedPeriods[3]
    endTimeStamp = 1512172800  # Saturday, December 2, 2017 12:00:00 AM (GMT)
    numberOfDays = 100
    startTimeStamp = endTimeStamp - 60*60*24*numberOfDays

    dataProcessor = PoloniexDataProcessor(currencyPairs)
    dataProcessor.getHistoricalData(currencyPairs, startTimeStamp, endTimeStamp, period)

    # to build up the probability matrices in the Betting class, we first get downSameUp data for half of the historical data
    # the other half of the historical data will be used to see the outcome of simulated real-time betting
    downSameUpDataForCurrencies = dataProcessor.processHistoricalData(endTimeStamp - 60*60*24*(numberOfDays/2))

    tradingFee = 0.0025  # 0.25% is actually the highest a fee would be on the Poloniex exchange, see https://www.poloniex.com/fees/
    # to-do: get current balances using returnCompleteBalances API call
    initialCurrencyPrices = dataProcessor.getInitialCurrencyPrices()
    currentBalances = dataProcessor.createFakeCurrentBalances(initialCurrencyPrices)

    # initialize a Betting class instance
    #betting = Betting(tradingFee, currencyPairNames, currentBalances)
    betting = Betting(0, currencyPairs, currentBalances)
    betting.initializeMatricesWithHistoricalData(downSameUpDataForCurrencies)

    # print out the class variables after processing the historical data (optionally, as a sanity check)
    betting.printClassVariables()

    # latestDownSameUpData = []
    # to-do: use returnCompleteBalances API call to get current available bitcoin in account

    initialBankroll = dataProcessor.calculateTotalValueOfBankroll(currentBalances)

    dataPlotter = DataPlotter(currencyPairs)

    bankrollValues = []
    bankrollValues.append(initialBankroll)

    timestamps = []
    timestamps.append(startTimeStamp + (dataProcessor.candlestickIndexToProcess * period))
    dates = []
    dates.append(datetime.datetime.fromtimestamp(timestamps[-1]))

    currentBalancesOverTime = {}
    currentBalancesOverTime['BTC'] = []
    currentBalancesOverTime['BTS'] = []
    currentBalancesOverTime['DASH'] = []
    currentBalancesOverTime['LTC'] = []
    currentBalancesOverTime['NXT'] = []
    currentBalancesOverTime['ETH'] = []
    currentBalancesOverTime['BCH'] = []

    for key in currentBalances:
        currentBalancesOverTime[key].append(float(currentBalances[key]['available']))


    while dataProcessor.candlestickIndexToProcess < len(dataProcessor.historicalData[0]):
        #time.sleep(300)  # to-do: figure out how often the ticker updates and the best interval to sleep(). we might want to subscribe to Poloniex ticker feed.
        #previousDownSameUpData = latestDownSameUpData
        #latestDownSameUpData = dataProcessor.getLatestDownSameUpData(currencyPairNames)
        #downSameUpData = []
        #downSameUpData.append(previousDownSameUpData)
        #downSameUpData.append(latestDownSameUpData)
        #if len(previousDownSameUpData) > 0:

        downSameUpData = dataProcessor.getNextDownSameUpDataPair()  # the dataProcessor class keeps track of which candlestick to process next

        if len(downSameUpData) > 0:
            #print('\n starting a round of buying/selling. current balances: ')
            #print(currentBalances)

            currentBalances = dataProcessor.updateBtcValueOnCurrentBalances(currentBalances)
            betting.calculateFractionOfBankrollInvestedFromCurrentBalances(currentBalances)

            bankrollValueBeforeTrading = dataProcessor.calculateTotalValueOfBankroll(currentBalances)
            #print('\n before trading this round, total value of your bankroll is %f' % bankrollValue)  # to-do: write this variable to a CSV file so we can plot it

            betting.conditionalProbability(downSameUpData)
            buy, sell = betting.buySell(bankrollValueBeforeTrading, False)

            currentBalances = dataProcessor.sellCurrencies(sell, currentBalances, tradingFee)  #NOTE: when using Poloniex, we don't need to keep track of currentBalances ourselves
            betting.calculateFractionOfBankrollInvestedFromCurrentBalances(currentBalances)

            currentBalances = dataProcessor.buyCurrencies(buy, currentBalances, tradingFee)
            betting.calculateFractionOfBankrollInvestedFromCurrentBalances(currentBalances)

            # add to arrays for data that we want to plot after the simulated betting is over
            bankrollValueAfterThisRound = dataProcessor.calculateTotalValueOfBankroll(currentBalances)
            bankrollValues.append(bankrollValueAfterThisRound)
            timestamps.append(startTimeStamp + (dataProcessor.candlestickIndexToProcess * period))
            dates.append(datetime.datetime.fromtimestamp(timestamps[-1]))

            for key in currentBalances:
                currentBalancesOverTime[key].append(float(currentBalances[key]['available']))

            #print('\n after trading this round, total value of your bankroll is %f' % bankrollValueAfterThisRound + '. recent change in bankroll is %f' % (bankrollValueAfterThisRound - bankrollValueBeforeTrading))


    #dataPlotter.generatePlot(timestamps, bankrollValues, '[epoch seconds]', '[bitcoins]', 'bankroll value over time')
    dataPlotter.generateDatePlot(dates, bankrollValues, '', '[bitcoins]', 'bankroll value over time')
    dataPlotter.generatePlotsFromYValuesDictionary(timestamps, currentBalancesOverTime, '[epoch seconds]', '[units owned]', 'amount owned of each coin over time')

    finalBankroll = dataProcessor.calculateTotalValueOfBankroll(currentBalances)
    initialBitcoinPriceInUsd = getHistoricBitcoinPrice(startTimeStamp)
    finalBitcoinPriceInUsd = getHistoricBitcoinPrice(endTimeStamp)

    print('\n ********************************** end of simulated betting **********************************')
    print('\n final bankroll: %f' % finalBankroll + ' bitcoins. initial bankroll: %f' % initialBankroll + ' bitcoins. net change: %f' % (finalBankroll-initialBankroll) + ' bitcoins')
    print('\n final bankroll: %f' % (finalBankroll*finalBitcoinPriceInUsd) + ' USD. initial bankroll: %f' % (initialBankroll*initialBitcoinPriceInUsd) + ' USD. net change: %f' % (finalBankroll*finalBitcoinPriceInUsd-initialBankroll*initialBitcoinPriceInUsd) + ' USD')

    # compare results from trading to what our bankroll value would be if we just held our initial investments
    balancesIfInitialInvestmentsWereHeld = dataProcessor.createFakeCurrentBalances(dataProcessor.latestCurrencyPrices)
    bankrollIfInitialInvestmentsWereHeld = dataProcessor.calculateTotalValueOfBankroll(balancesIfInitialInvestmentsWereHeld)
    print('\n if initial investments were held, you would have %f' % bankrollIfInitialInvestmentsWereHeld + ' bitcoins, which is %f' % (bankrollIfInitialInvestmentsWereHeld*finalBitcoinPriceInUsd) + ' USD')

    if bankrollIfInitialInvestmentsWereHeld > finalBankroll:
        print(' if initial investments were held, you would have %f' % ((bankrollIfInitialInvestmentsWereHeld - finalBankroll)*finalBitcoinPriceInUsd) + ' more dollars')

    print('\n total number of sell orders: %d' % dataProcessor.totalNumberOfSellOrders + ', total number of buy orders: %d' % dataProcessor.totalNumberOfBuyOrders)

    print('\n current balances: ')
    print(currentBalances)

    print('\n currencyPairs: ')
    print(currencyPairs)
    print('\n initial currency prices in terms of bitcoins: ')
    print(initialCurrencyPrices)

    initialCurrencyPricesInUsd = []
    for currencyPriceInBitcoins in initialCurrencyPrices:
        initialCurrencyPricesInUsd.append(currencyPriceInBitcoins*initialBitcoinPriceInUsd)

    print('\n initial currency prices in USD: ')
    print(initialCurrencyPricesInUsd)

    print('\n latest currency prices in terms of bitcoins: ')
    print(dataProcessor.latestCurrencyPrices)

    finalCurrencyPricesInUsd = []
    for currencyPriceInBitcoins in dataProcessor.latestCurrencyPrices:
        finalCurrencyPricesInUsd.append(currencyPriceInBitcoins*finalBitcoinPriceInUsd)

    print('\n final currency prices in USD: ')
    print(finalCurrencyPricesInUsd)


# we probably don't actually need this function...
def getCurrentBitcoinPrice():
    response = urllib.request.urlopen('https://bitpay.com/api/rates').read()  # this site is updated once per minute
    responseJson = json.loads(response)
    bitcoinPriceInUsDollars = responseJson[1]['rate']
    return bitcoinPriceInUsDollars

def getHistoricBitcoinPrice(timestamp):
    response = urllib.request.urlopen('https://index.bitcoin.com/api/v0/lookup?time=' + str(timestamp)).read()
    responseJson = json.loads(response)
    bitcoinPrice = responseJson['open']['price']/100.0  # the price is given in cents, so convert to dollars
    return float(bitcoinPrice)

if __name__ == "__main__":
    main(sys.argv[1:])
