import matplotlib.dates as mdates
import matplotlib.pyplot as pyplot
import os

class DataPlotter:
    currencyPairs = []

    def __init__(self, currencyPairs):
        self.currencyPairs = currencyPairs

    def generatePlot(self, xValues, yValues, xAxisLabel = '', yAxisLabel = '', plotTitle = '', fileName = 'myplot.png', saveFigure = False):
        figure, axes = pyplot.subplots()

        axes.plot(xValues, yValues)
        axes.grid(True)
        axes.set_title(plotTitle)
        axes.set_xlabel(xAxisLabel)
        axes.set_ylabel(yAxisLabel)

        if saveFigure:
            pyplot.savefig(os.sep.join([os.path.expanduser('~'), 'Desktop', fileName]))

        pyplot.show(block=True)

    def generateDatePlot(self, xValueDates, yValues, xAxisLabel = '', yAxisLabel = '', plotTitle = '', fileName = 'myplot.png', saveFigure = False):
        figure, axes = pyplot.subplots()

        axes.plot(xValueDates, yValues)
        axes.grid(True)
        axes.set_title(plotTitle)
        axes.set_xlabel(xAxisLabel)
        axes.set_ylabel(yAxisLabel)

        days = mdates.DayLocator()
        dateFormat = mdates.DateFormatter('%m-%d')

        # format the ticks
        axes.xaxis.set_major_locator(days)
        axes.xaxis.set_major_formatter(dateFormat)
        pyplot.xticks(rotation=90)

        if saveFigure:
            pyplot.savefig(os.sep.join([os.path.expanduser('~'), 'Desktop', fileName]))

        pyplot.show(block=True)  # this pauses the program until you close the plot window

        pyplot.xticks(rotation=0)  # reset this


    def generatePlotsFromYValuesDictionary(self, xValues, yValuesDataDictionary, xAxisLabel = '', yAxisLabel = '', plotTitle = '', fileName = 'myplot.png', saveFigure = False):
        figure, axes = pyplot.subplots()

        for key in yValuesDataDictionary:
            axes.plot(xValues, yValuesDataDictionary[key], label=key)

        axes.grid(True)
        axes.set_title(plotTitle)
        axes.set_xlabel(xAxisLabel)
        axes.set_ylabel(yAxisLabel)
        axes.set_yscale('log')

        pyplot.legend()

        if saveFigure:
            pyplot.savefig(os.sep.join([os.path.expanduser('~'), 'Desktop', fileName]))

        pyplot.show(block=True)
        