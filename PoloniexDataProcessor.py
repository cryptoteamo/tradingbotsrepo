import time
from Poloniex import Poloniex


class PoloniexDataProcessor:

    publicKey = ''  # once we have a Poloniex account, we will hard code this key into this file
    privateKey = ''  # once we have a Poloniex account, we will hard code this key into this file
    poloniex = Poloniex(publicKey, privateKey)
    historicalData = []  # each element of this array is a (giant) list of candlesticks for one currency. elements are ordered by currencyPairNames.
    currencyPairs = []
    latestCurrencyPrices = []
    candlestickIndexToProcess = 0
    totalNumberOfBuyOrders = 0
    totalNumberOfSellOrders = 0
    initialCurrencyPrices = []

    def __init__(self, currencyPairs):
        self.currencyPairs = currencyPairs

    def getCurrencyPairsAboveThreshold(self, volumeThresholdInUnitsOfBitcoin):
        # NOTE: we don't want to consider every currency, only those above a specified threshold of volume traded in the last 24 hours
        dayVolume = self.poloniex.return24hVolume()  # dictionary of currency pairs, for example: "BTC_LTC":{"BTC":"3025.45263882","LTC":"313293.77364544"}
        allBtcCurrencyPairs = []
        currencyPairsAboveVolumeThreshold = []

        for key in dayVolume:
            if "BTC_" in key:
                allBtcCurrencyPairs.append(key)

        for currencyPair in allBtcCurrencyPairs:
            if float(dayVolume[currencyPair]['BTC']) > volumeThresholdInUnitsOfBitcoin:
                currencyPairsAboveVolumeThreshold.append(currencyPair)

        return currencyPairsAboveVolumeThreshold

    def printTickerDataForCurrencyPair(self, currencyPairName):
        # returnTicker gives currency pairs with data: {id, last, lowestAsk, highestBid, percentChange, baseVolume, quoteVolume, isFrozen, high24hr, low24hr}
        tickerData = self.poloniex.returnTicker()
        print('\n current ticker information for currencyPair ' + currencyPairName + ':')
        print('   last price: ' + tickerData[currencyPairName]['last'])
        print('   recent percentage change: ' + tickerData[currencyPairName]['percentChange'])
        print('   lowest ask price: ' + tickerData[currencyPairName]['lowestAsk'])
        print('   highest bid price: ' + tickerData[currencyPairName]['highestBid'])

    def print24HourVolumeDataForBitcoinInUsDollars(self):
        dayVolume = self.poloniex.return24hVolume()  # this is a dictionary of currency pairs
        averagePriceBitcoin = float(dayVolume['USDT_BTC']['USDT']) / float(dayVolume['USDT_BTC']['BTC'])
        print('\n 24 hour volume on Poloniex exchange')
        print('   {USDT : BTC} = {' + dayVolume['USDT_BTC']['USDT'] + ' : ' + dayVolume['USDT_BTC']['BTC'] + '}')
        print('      average price (roughly) of bitcoin in US dollars: %.2f' % averagePriceBitcoin)

    def getHistoricalData(self, currencyPairNames, startTimeStamp, endTimeStamp, periodInSeconds):
        for currencyPairName in currencyPairNames:
            parameters = {"currencyPair": currencyPairName, "start": startTimeStamp, "end": endTimeStamp, "period": periodInSeconds}
            print('getting historical data for currency pair ' + currencyPairName + ' from %d' % startTimeStamp + ' to %d' % endTimeStamp)
            time.sleep(0.25)  # Poloniex doesn't want us making more than 6 API calls/second. this line ensures we don't exceed 4 calls/second
            historicalData = self.poloniex.apiQuery("returnChartData", parameters)  # returns candlestick data: {date, high, low, open, close, volume, quoteVolume, weightedAverage}
            self.historicalData.append(historicalData)

    def processHistoricalData(self, endTimeStamp):
        # process the cached historical data from the first candlestick up to the endTimeStamp
        downSameUpForAllCurrencies = []

        for historicalDataForOneCurrency in self.historicalData:
            downSameUpForThisCurrency = []
            for candlestick in historicalDataForOneCurrency:
                if candlestick['date'] > endTimeStamp:
                    break
                downSameUpForThisCurrency.append(self.processCandlestick(candlestick))  # appends -1, 0, or 1

            downSameUpForAllCurrencies.append(downSameUpForThisCurrency)

        self.candlestickIndexToProcess = len(downSameUpForAllCurrencies[0])  # this is useful to keep track of!

        return downSameUpForAllCurrencies

    def getNextDownSameUpDataPair(self):
        # this method is for running simulated betting only
        downSameUpForAllCurrencies = []
        self.latestCurrencyPrices = []

        for historicalDataForOneCurrency in self.historicalData:
            downSameUpForThisCurrency = []
            downSameUpForThisCurrency.append(self.processCandlestick(historicalDataForOneCurrency[self.candlestickIndexToProcess-1]))  # get previous
            downSameUpForThisCurrency.append(self.processCandlestick(historicalDataForOneCurrency[self.candlestickIndexToProcess]))  # get current
            downSameUpForAllCurrencies.append(downSameUpForThisCurrency)

            self.latestCurrencyPrices.append(historicalDataForOneCurrency[self.candlestickIndexToProcess]['close'])  # keep track of this for simulated buying/selling

        self.candlestickIndexToProcess += 1

        # Next, we need to reformat this data in a way that the Betting class can use

        reformattedCurrent = []
        reformattedPrevious = []

        isThereAnErrorWithData = False

        for downSameUpData in downSameUpForAllCurrencies:
            if len(downSameUpData) < 2:
                isThereAnErrorWithData = True
                break
            reformattedPrevious.append(downSameUpData[0])
            reformattedCurrent.append(downSameUpData[1])

        reformattedDownSameUpData = []
        reformattedDownSameUpData.append(reformattedPrevious)
        reformattedDownSameUpData.append(reformattedCurrent)

        if isThereAnErrorWithData:
            return []
        else:
            return reformattedDownSameUpData

    def processCandlestick(self, candlestick):
        change = candlestick['close'] - candlestick['open']
        if change < 0:
            return -1
        elif change > 0:
            return 1
        else:
            return 0

    def getLatestDownSameUpData(self, currencyPairNames):
        tickerData = self.poloniex.returnTicker()
        latestDownSameUpData = []

        for currencyPairName in currencyPairNames:
            percentChange = float(tickerData[currencyPairName]['percentChange'])
            if percentChange < 0:
                latestDownSameUpData.append(-1)
            elif percentChange > 0:
                latestDownSameUpData.append(1)
            else:
                latestDownSameUpData.append(0)

        return latestDownSameUpData

    def sellCurrencies(self, currencyPairsToSellWithAmounts, currentBalances, tradingFee):
        # example of currencyPairsToSellWithAmount: [['BTC_XMR', -0.0405155],['BTC_DASH', -0.228919]]
        if len(currencyPairsToSellWithAmounts) == 0:
            #print('\n nothing to sell this round')
            return currentBalances

        # tickerData = self.poloniex.returnTicker()  # to-do: use this during live betting

        for currencyPairWithAmountToSell in currencyPairsToSellWithAmounts:
            # lastCurrencyPairPrice = float(tickerData[currencyPair]['last'])
            # lowestAskPrice = float(tickerData[currencyPair[currencyPair]['lowestAsk'])

            # to-do: figure out how to use the last, lowestAsk, and highestBid values to determine the best price to use in a sell order
            # to-do: get current fee using the returnFeeInfo, and use either the maker or taker value

            currencyPair = currencyPairWithAmountToSell[0]
            amountToSell = abs(currencyPairWithAmountToSell[1])  # in units of bitcoin. NOTE: the abs() is needed because the amount to sell is a negative number
            # priceToSellAt = float(tickerData[currencyPair]['highestBid'])  # to-do: use this for live betting or live simulated betting
            currencyPairIndex = self.findIndex(self.currencyPairs, currencyPair)
            priceToSellAt = self.latestCurrencyPrices[currencyPairIndex]
            currencyName = currencyPair[4:]  # example: 'BTC_ETH' would substring to 'ETH'

            if float(currentBalances[currencyName]['btcValue']) >= amountToSell:
                # to-do: put in a sell order using the API call "sell"
                self.totalNumberOfSellOrders += 1
                # assuming the sell order was immediately filled (NOT a good assumption, cuz this will never happen!) add to our cash and balances
                currentBalances[currencyName]['btcValue'] = str(float(currentBalances[currencyName]['btcValue']) - amountToSell)
                currentBalances[currencyName]['available'] = str(float(currentBalances[currencyName]['available']) - amountToSell/priceToSellAt)  # in units of this currency, not bitcoin
                currentBalances['BTC']['available'] = str(float(currentBalances['BTC']['available']) + amountToSell*(1.0 - tradingFee))
                currentBalances['BTC']['btcValue'] = currentBalances['BTC']['available']
                #print('sold %f' % amountToSell + ' of ' + currencyPair + ' at price %f' % priceToSellAt)
            #else:
                #print('ERROR: insufficient funds for currency ' + currencyName + '. Current amount is ' + currentBalances[currencyName]['btcValue'] + ', but we wanted to sell %f' % amountToSell)

        return currentBalances

    def buyCurrencies(self, currencyPairsToBuyWithAmounts, currentBalances, tradingFee):
        # example of currencyPairsToSellWithAmount: [['BTC_XMR', 0.0405155],['BTC_DASH', 0.228919]]
        if len(currencyPairsToBuyWithAmounts) == 0:
            #print('\n nothing to buy this round')
            return currentBalances

        # tickerData = self.poloniex.returnTicker()  # to-do: use this when doing live betting

        for currencyPairWithAmountToBuy in currencyPairsToBuyWithAmounts:
            currencyPair = currencyPairWithAmountToBuy[0]
            amountToBuy = currencyPairWithAmountToBuy[1]  # this is the fraction of your bankroll to use to buy this currency
            #lowestAskPrice = float(tickerData[currencyPair]['lowestAsk'])  # doing this makes you the "Taker", and this means higher fees, but you might get to buy immediately instead of waiting to get matched up
            currencyPairIndex = self.findIndex(self.currencyPairs, currencyPair)
            priceToBuyAt = self.latestCurrencyPrices[currencyPairIndex]
            currencyName = currencyPair[4:]  # example: BTC_ETH becomes ETH

            bankroll = self.calculateTotalValueOfBankroll(currentBalances)

            if float(currentBalances[currencyName]['available']) >= amountToBuy*priceToBuyAt*(1.0 + tradingFee):
                # to-do: place a buy order
                self.totalNumberOfBuyOrders += 1
                currentBalances[currencyName]['btcValue'] = str(float(currentBalances[currencyName]['btcValue']) + amountToBuy)
                currentBalances[currencyName]['available'] = str(float(currentBalances[currencyName]['available']) + amountToBuy/priceToBuyAt)  # in units of this currency, not bitcoin
                currentBalances['BTC']['available'] = str(float(currentBalances['BTC']['available']) - amountToBuy*(1.0 - tradingFee))
                currentBalances['BTC']['btcValue'] = currentBalances['BTC']['available']
                #print('bought %f' % amountToBuy + ' of ' + currencyPair + ' at price %f' % lowestAskPrice)
            #else:
                # to-do: should we buy as much as we can afford? or don't buy any of this currency this round?
                #print('ERROR: insufficient funds to buy ' + currencyName + '. Attempting to buy %f' % amountToBuy + ', but only have a cash balance of ' + currentBalances['BTC']['available'])

        return currentBalances

    def calculateTotalValueOfBankroll(self, currentBalances):
        btcValueOfAllAssets = 0.0  # in units of bitcoin
        for key in currentBalances:
            btcValueOfThisCurrency = float(currentBalances[key]['btcValue'])
            btcValueOfAllAssets += btcValueOfThisCurrency

        return btcValueOfAllAssets

    def calculateTotalValueOfBankrollInUsd(self, currentBalances, bitcoinPriceInUsd):
        btcValueOfAllAssets = 0.0  # in units of bitcoin
        for key in currentBalances:
            btcValueOfThisCurrency = float(currentBalances[key]['btcValue'])
            btcValueOfAllAssets += btcValueOfThisCurrency

        return btcValueOfAllAssets*bitcoinPriceInUsd

    def updateBtcValueOnCurrentBalances(self, currentBalances):
        currentBalances['BTS']['btcValue'] = str(float(self.latestCurrencyPrices[0])*float(currentBalances['BTS']['available']))
        currentBalances['DASH']['btcValue'] = str(float(self.latestCurrencyPrices[1])*float(currentBalances['DASH']['available']))
        currentBalances['LTC']['btcValue'] = str(float(self.latestCurrencyPrices[2])*float(currentBalances['LTC']['available']))
        currentBalances['NXT']['btcValue'] = str(float(self.latestCurrencyPrices[3])*float(currentBalances['NXT']['available']))
        currentBalances['ETH']['btcValue'] = str(float(self.latestCurrencyPrices[4])*float(currentBalances['ETH']['available']))
        currentBalances['BCH']['btcValue'] = str(float(self.latestCurrencyPrices[5])*float(currentBalances['BCH']['available']))
        return currentBalances

    def createFakeCurrentBalances(self, currencyPrices):
        currentBalances = {}
        currentBalances['BTC'] = {'available':'0.5','onOrders':'0.0','btcValue':'0.5'}
        currentBalances['BTS'] = {'available':'0.0','onOrders':'0.0','btcValue':str(float(currencyPrices[0])*0.0)}
        currentBalances['DASH'] = {'available':'6.0','onOrders':'0.0','btcValue':str(float(currencyPrices[1])*6.0)}
        currentBalances['LTC'] = {'available':'9.0','onOrders':'0.0','btcValue':str(float(currencyPrices[2])*9.0)}
        currentBalances['NXT'] = {'available':'100.0','onOrders':'0.0','btcValue':str(float(currencyPrices[3])*100.0)}
        currentBalances['ETH'] = {'available':'2.0','onOrders':'0.0','btcValue':str(float(currencyPrices[4])*2.0)}
        currentBalances['BCH'] = {'available':'0.0','onOrders':'0.0','btcValue':str(float(currencyPrices[5])*0.0)}
        return currentBalances

    def getInitialCurrencyPrices(self):
        initialCurrencyPrices = []
        for historicalDataForOneCurrency in self.historicalData:
            initialCurrencyPrices.append(historicalDataForOneCurrency[self.candlestickIndexToProcess]['open'])

        return initialCurrencyPrices

    def findIndex(self, array, valueToFind):
        for i in range(len(array)):
            if array[i] == valueToFind :
                return i

        return -1